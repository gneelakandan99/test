April 5 starts an internship session. Tazeen ,my mentor asked me to start with html css and css in inline css,internal css,external css.
Css:
Cascading Style Sheets (CSS) is used to format the layout of a webpage.
With CSS, you can control the color, font, the size of text, the spacing between elements, how elements are positioned and laid out, what background images or background colors are to be used, different displays for different devices and screen sizes.
Css using 
-Inline
-Internal
-external
Inline using the style attribute in html
You can easily and quickly insert CSS rules to an HTML page. That's why this method is useful for testing or previewing the changes, and performing quick-fixes to your website.
You don't need to create and upload a separate document as in the external style.
 internal by using a <style> element in the <head> section
You can integrate internal CSS stylesheets by placing the <style>element in the <head>section of a page. Internal styles apply to whole pages but not to multiple HTML documents. Several pages can be styled by repeating the same block of internal styles in them
 external by using a <link> element to link to an external CSS file.
The external style sheet is generally used when you want to make changes on multiple pages. It is ideal for this condition because it facilitates you to change the look of the entire web site by changing just one file. It uses the <link> tag on every page and the <link> tag should be put inside the head section.
Css comment:
Comments are used to explain the code, and may help when you edit the source code at a later date.
Comments are ignored by browsers.
A CSS comment is placed inside the <style> element, and starts with /* and ends with */:
position
The CSS position property defines the position of an element in a document. This property works with the left, right, top, bottom and z-index properties to determine the final position of an element on a page.
The absolute positioning is best if you need something to be placed at an exact coordinate. The relative positioning will take where the element already is, and add the coordinates to it
Fixed.-An element with position: fixed; is positioned relative to the viewport, which means it always stays in the same place even if the page is scrolled. The top, right, bottom, and left properties are used to position the element.
A fixed element does not leave a gap in the page where it would normally have been located.
Notice the fixed element in the lower-right corner of the page. 
Static.-HTML elements are positioned static by default.
Static positioned elements are not affected by the top, bottom, left, and right properties.
An element with position: static; is not positioned in any special way; it is always positioned according to the normal flow of the page:
his <div> element has position: static;
Relatives.-An element with position: relative; is positioned relative to its normal position.
Setting the top, right, bottom, and left properties of a relatively-positioned element will cause it to be adjusted away from its normal position. Other content will not be adjusted to fit into any gap left by the element.
This <div> element has position: relative;

Absolute.-An element with position: absolute; is positioned relative to the nearest positioned ancestor (instead of positioned relative to the viewport, like fixed).
However; if an absolute positioned element has no positioned ancestors, it uses the document body, and moves along with page scrolling.

             Sticky.-An element with position: sticky; is positioned based on the user's scroll position.
A sticky element toggles between relative and fixed, depending on the scroll position. It is positioned relative until a given offset position is met in the viewport - then it "sticks" in place 

Property
Description
bottom
Sets the bottom margin edge for a positioned box
clip
Clips an absolutely positioned element
left
Sets the left margin edge for a positioned box
position
Specifies the type of positioning for an element
right
Sets the right margin edge for a positioned box
top
Sets the top margin edge for a positioned box


Today my internship was over.




